package com.alexandro45.policekiller.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

/**
 * Created by Саша on 29.11.2016.
 *
 */

public class StateManager {

    private Stack<State> states;

    public StateManager(){
        states = new Stack<State>();
    }

    public void push(State state){
        states.push(state);
    }

    public void pop(){
        states.pop().dispose();
    }

    void set(State state){
        states.pop().dispose();
        states.push(state);
    }

    public void update(){
        states.peek().update();
    }

    public void render(SpriteBatch batch){
        states.peek().render(batch);
    }
}
