package com.alexandro45.policekiller.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * Created by Саша on 29.11.2016.
 *
 */

public class Start extends State{

    private Texture playBtn;
    private BitmapFont score;
    private BitmapFont result;
    private final int SCREEN_WIDTH = 640;
    private final int SCREEN_HEIGHT = 480;
    private File file;
    private File file2;
    private String scoreS;
    private String resultS;

    public Start(StateManager stateManager) {
        super(stateManager);
        camera.setToOrtho(false,640,480);
        playBtn = new Texture("play.png");
        score = new BitmapFont();
        result = new BitmapFont();
        file = new File(Gdx.files.getExternalStoragePath() + ".policekiller/score.dat");
        file2 = new File(Gdx.files.getExternalStoragePath() + ".policekiller/result.dat");
        if(!file.exists()) {
            new File(Gdx.files.getExternalStoragePath() + ".policekiller/").mkdir();
            try {
                file.createNewFile();
                file2.createNewFile();
                PrintWriter pw = new PrintWriter(new FileOutputStream(file));
                pw.println(0);
                pw.close();
                PrintWriter pw2 = new PrintWriter(new FileOutputStream(file2));
                pw2.println(0);
                pw2.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        scoreS = getScore();
        resultS = getResult();
    }

    @Override
    public void render(SpriteBatch batch) {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(playBtn,SCREEN_WIDTH/2-playBtn.getWidth()/2,SCREEN_HEIGHT/2-playBtn.getHeight()/2);
        score.draw(batch,scoreS,SCREEN_WIDTH/2-score.getLineHeight()/2,310);
        result.draw(batch,resultS,SCREEN_WIDTH/2-result.getLineHeight()/2,325);
        batch.end();
        if (Gdx.input.isTouched()){
            input.set(Gdx.input.getX(),Gdx.input.getY(),0);
            camera.unproject(input);
            if(input.x >= SCREEN_WIDTH/2-playBtn.getWidth()/2 &&
                    input.y >= SCREEN_HEIGHT/2-playBtn.getHeight()/2 &&
                    input.x <= SCREEN_WIDTH/2+playBtn.getWidth()/2 &&
                    input.y <= SCREEN_HEIGHT/2+playBtn.getHeight()/2){
                stateManager.set(new Game(stateManager));
            }
        }
    }

    private String getScore(){
        String score = "";
        try {
            score = new BufferedReader(new InputStreamReader(new FileInputStream(file))).readLine();
            if(score == null) score = "0";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return score;
    }

    private String getResult(){
        String result = "";
        try {
            result = new BufferedReader(new InputStreamReader(new FileInputStream(file2))).readLine();
            if(result == null) result = "0";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void update() {

    }

    @Override
    public void dispose() {
        playBtn.dispose();
        score.dispose();
        result.dispose();
    }
}
