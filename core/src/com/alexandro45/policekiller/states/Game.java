package com.alexandro45.policekiller.states;

import com.alexandro45.policekiller.graphics.Police;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Саша on 29.11.2016.
 *
 */

class Game extends State{

    private Texture hero,police,track,bullet;
    private Rectangle rHero,rTrack,rBullet;
    private final int police_count = 3;
    private ArrayList<Police> polices = new ArrayList<Police>();
    private int tick = 0;
    private boolean isAtack = false;
    private Random random = new Random();
    private Vector3 vector3 = new Vector3();
    private Sound removeS,policeD;
    private int score = 0;
    private int loser = 0;
    private BitmapFont text;

    Game(StateManager stateManager) {
        super(stateManager);
        camera.setToOrtho(false,640,480);
        hero = new Texture("hero.png");
        police = new Texture("police.png");
        track = new Texture("track.png");
        bullet = new Texture("bullet.png");
        removeS = Gdx.audio.newSound(Gdx.files.internal("explosion.wav"));
        policeD = Gdx.audio.newSound(Gdx.files.internal("police_destroy.wav"));
        text = new BitmapFont();

        for (int i = 0;i < police_count;i++){
            Rectangle r = new Rectangle();
            r.x = 640/2;
            r.y = 380;
            r.width = police.getWidth();
            r.height = police.getHeight();

            float speed = random.nextFloat()*3;
            boolean dir = random.nextBoolean();

            polices.add(new Police(dir,speed,r));
        }

        rHero = new Rectangle();
        rHero.x = 640/2;
        rHero.y = 5;
        rHero.width = hero.getWidth();
        rHero.height = hero.getHeight();

        rTrack = new Rectangle();
        rTrack.x = 0;
        rTrack.y = 370;
        rTrack.width = 680;
        rTrack.height = track.getHeight();
    }

    @Override
    public void render(SpriteBatch batch) {
        try {
            batch.setProjectionMatrix(camera.combined);
            batch.begin();
            batch.draw(hero, rHero.getX(), rHero.getY());
            batch.draw(track,rTrack.getX(),rTrack.getY());
            for (Police p:polices){
                batch.draw(police,p.getR().getX(),p.getR().getY());
            }
            if(isAtack) batch.draw(bullet,rHero.getX() + (rHero.getWidth()/2),rBullet.getY());
            text.draw(batch,"Shot down : " + score + " Missed : " + loser,20,20);
            batch.end();
        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            if (Gdx.input.isTouched()){
                vector3.set(Gdx.input.getX(),hero.getWidth(),0);
                camera.unproject(vector3);
                rHero.x = vector3.x - (hero.getWidth()/2);
                isAtack = true;
                rBullet = new Rectangle();
                rBullet.x = rHero.getX()+(rHero.getWidth()/2);
                rBullet.y = 20;
                rBullet.width = bullet.getWidth();
                rBullet.height = bullet.getHeight();
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        try {
            for (Police p:polices){
                if (p.isDir()){
                    p.getR().x += p.getSpeed();
                    if(p.getR().getX() > 640){
                        polices.remove(p);
                        policeD.play();
                        loser++;
                    }
                }else{
                    p.getR().x -= p.getSpeed();
                    if(p.getR().getX()+p.getR().getWidth() < 0){
                        polices.remove(p);
                        policeD.play();
                        loser++;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        if(loser == 20){
            gameOver();
        }

        if(isAtack){
            rBullet.y += 30;
            System.out.println(rBullet.getY());
            if(rBullet.getY() > 480){
                rBullet = null;
                isAtack = false;
            }
        }

        try {
            if (isAtack) {
                for (Police p : polices) {
                    if (rBullet.overlaps(p.getR())) {
                        polices.remove(p);
                        System.out.println("tt");
                        rBullet = null;
                        isAtack = false;
                        System.out.println("remowe");
                        removeS.play();
                        score++;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        tick++;
        if(tick == 50){
            tick = 0;
            Rectangle r = new Rectangle();
            r.x = random.nextInt(200)+200;
            r.y = 380;
            r.width = police.getWidth();
            r.height = police.getHeight();

            float speed = random.nextFloat()*3;
            boolean dir = random.nextBoolean();

            polices.add(new Police(dir,speed,r));
        }
    }

    private void gameOver(){
        try {
            for (Police p:polices){
                polices.remove(p);
                rHero = null;
                rBullet = null;
                rTrack = null;
                File file = new File(Gdx.files.getExternalStoragePath() + ".policekiller/score.dat");
                File file2 = new File(Gdx.files.getExternalStoragePath() + ".policekiller/result.dat");
                if(!file.exists()) {
                    new File(Gdx.files.getExternalStoragePath() + ".policekiller/").mkdir();
                    file.createNewFile();
                    file2.createNewFile();

                    PrintWriter pw = new PrintWriter(new FileOutputStream(file));
                    pw.println(score);
                    pw.close();
                }else if(Integer.parseInt(new BufferedReader(new InputStreamReader(new FileInputStream(file))).readLine()) < score){
                    PrintWriter pw = new PrintWriter(new FileOutputStream(file));
                    pw.println(score);
                    pw.close();
                }else{
                    PrintWriter pw = new PrintWriter(new FileOutputStream(file2));
                    pw.println(score);
                    pw.close();
                }
                System.out.println("end");
                stateManager.set(new Start(stateManager));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void update() {

    }

    @Override
    public void dispose() {
        hero.dispose();
        police.dispose();
        track.dispose();
        bullet.dispose();
        text.dispose();
        removeS.dispose();
        policeD.dispose();
    }
}
