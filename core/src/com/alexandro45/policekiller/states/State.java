package com.alexandro45.policekiller.states;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by Саша on 29.11.2016.
 *
 */

abstract class State {

    OrthographicCamera camera;
    Vector3 input;
    StateManager stateManager;

    State(StateManager stateManager){
        this.stateManager = stateManager;
        camera = new OrthographicCamera();
        input = new Vector3();
    }

    public abstract void render(SpriteBatch batch);
    public abstract void update();
    public abstract void dispose();
}
