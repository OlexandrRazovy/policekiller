package com.alexandro45.policekiller;

import com.alexandro45.policekiller.states.Start;
import com.alexandro45.policekiller.states.StateManager;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Main extends ApplicationAdapter {

	private SpriteBatch batch;
	private StateManager stateManager = new StateManager();

	@Override
	public void create () {
		batch = new SpriteBatch();
		Gdx.gl.glClearColor(0.19f, 1, 0.31f, 1);
		stateManager.push(new Start(stateManager));
	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stateManager.render(batch);
	}

	@Override
	public void dispose () {
		stateManager.pop();
		batch.dispose();
	}
}
