package com.alexandro45.policekiller.graphics;

import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Саша on 18.11.2016.
 *
 */

public class Police {

    private Rectangle r;
    private float speed;
    private boolean dir;

    public Police(boolean dir, float speed, Rectangle r) {
        this.dir = dir;
        this.speed = speed;
        this.r = r;
    }

    public Rectangle getR() {
        return r;
    }

    public float getSpeed() {
        return speed;
    }

    public boolean isDir() {
        return dir;
    }
}
